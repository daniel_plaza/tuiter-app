package com.kairosds.tuiter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kairosds.tuiter.dto.HashtagDTO;
import com.kairosds.tuiter.repository.HashtagRepository;
import com.kairosds.tuiter.util.DummyGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class HashtagControllerImplTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HashtagRepository hashtagRepository;

    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void readAllShouldReturn200() throws Exception {
        //GIVEN
        hashtagRepository.deleteAll();
        IntStream.range(0, 20).boxed()
                .map(i -> DummyGenerator.generateNewHashtag())
                .forEach(hashtag -> hashtagRepository.save(hashtag));

        //WHEN
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.get("/hashtags")
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        List<HashtagDTO> result = objectMapper.readValue(response.getContentAsByteArray(), List.class);

        //THEN
        assertEquals(200, response.getStatus());
        assertEquals(10, result.size());
    }

}
