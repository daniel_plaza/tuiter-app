package com.kairosds.tuiter.util;

import com.kairosds.tuiter.domain.Hashtag;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class DummyGenerator {

    private DummyGenerator() {
    }

    public static Hashtag generateNewHashtag() {
        return Hashtag.builder()
                .id(null)
                .value(UUID.randomUUID().toString())
                .count(0)
                .build();
    }

    public static Hashtag generateHashtag() {
        return Hashtag.builder()
                .id(UUID.randomUUID().toString())
                .value(String.valueOf(random(0, 1000)))
                .count(Long.valueOf(random(0, 10)))
                .build();
    }

    public static int random(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

}
