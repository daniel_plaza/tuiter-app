package com.kairosds.tuiter.service;

import com.kairosds.tuiter.domain.Hashtag;
import com.kairosds.tuiter.dto.HashtagDTO;
import com.kairosds.tuiter.repository.HashtagRepository;
import com.kairosds.tuiter.service.impl.HashtagServiceImpl;
import com.kairosds.tuiter.util.DummyGenerator;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HashTagServiceImplTest {

    @Mock
    private HashtagRepository hashtagRepository;

    @Spy
    private MapperFacade mapper = new DefaultMapperFactory.Builder()
            .build()
            .getMapperFacade();

    @InjectMocks
    private HashtagServiceImpl underTest;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(underTest, "hashtags", 10);
    }

    @Test
    public void readFirstTest() {
        //GIVEN
        List<Hashtag> hashtags = IntStream.range(0, 6).boxed()
                .map(i -> DummyGenerator.generateHashtag())
                .collect(Collectors.toList());

        when(hashtagRepository.findByOrderByCountDesc(any()))
                .thenReturn(hashtags);

        //WHEN
        List<HashtagDTO> result = underTest.readFirst();

        //THEN
        assertEquals(hashtags.size(), result.size());
        assertTrue(hashtags.stream()
                .map(Hashtag::getValue)
                .collect(Collectors.toSet())
                .containsAll(result.stream()
                        .map(HashtagDTO::getValue)
                        .collect(Collectors.toList())));
    }


}
