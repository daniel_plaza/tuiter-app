package com.kairosds.tuiter.exception.controller;

import com.kairosds.tuiter.dto.ExceptionDTO;
import com.kairosds.tuiter.exception.TweetNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(TweetNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionDTO tweetNotFound(TweetNotFoundException e) {
        return ExceptionDTO.builder()
                .message("Tweet(" + e.getId() + ") not found")
                .timestamp(LocalDateTime.now())
                .build();
    }

}
