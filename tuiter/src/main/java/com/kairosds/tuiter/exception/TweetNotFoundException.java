package com.kairosds.tuiter.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class TweetNotFoundException extends RuntimeException {

    @Getter
    private String id;

}
