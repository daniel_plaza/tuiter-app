package com.kairosds.tuiter.controller;

import com.kairosds.tuiter.dto.TweetDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Api
public interface TweetController {

    @ApiOperation(value = "Return stored tweets")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Tweets")})
    List<TweetDTO> readAll(Pageable page);

    @ApiOperation(value = "Return tweets validated of an user")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Tweets")})
    List<TweetDTO> readValidatedByUser(String user, Pageable page);

    @ApiOperation(value = "Return an specific Tweet")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Tweet"),
            @ApiResponse(code = 404, message = "Tweet not found")})
    TweetDTO readById(String id);

    @ApiOperation(value = "Validate a tweet")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validated"),
            @ApiResponse(code = 404, message = "Tweet not found")})
    TweetDTO validate(String id);

}
