package com.kairosds.tuiter.controller;

import com.kairosds.tuiter.dto.HashtagDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

@Api
public interface HashtagController {

    @ApiOperation(value = "Return first 10 hastags with more occurrences")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Hastags")})
    List<HashtagDTO> readAll();

}
