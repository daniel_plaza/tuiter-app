package com.kairosds.tuiter.controller.impl;

import com.kairosds.tuiter.controller.HashtagController;
import com.kairosds.tuiter.dto.HashtagDTO;
import com.kairosds.tuiter.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/hashtags")
public class HashtagControllerImpl implements HashtagController {

    @Autowired
    private HashtagService hashtagService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<HashtagDTO> readAll() {
        return hashtagService.readFirst();
    }

}
