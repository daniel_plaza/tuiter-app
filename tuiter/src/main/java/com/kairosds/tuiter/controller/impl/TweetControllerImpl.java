package com.kairosds.tuiter.controller.impl;

import com.kairosds.tuiter.controller.TweetController;
import com.kairosds.tuiter.dto.TweetDTO;
import com.kairosds.tuiter.service.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tweets")
public class TweetControllerImpl implements TweetController {

    @Autowired
    private TweetService tweetService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TweetDTO> readAll(Pageable page) {
        return tweetService.readAll(page);
    }

    @GetMapping(params = {"user"})
    @ResponseStatus(HttpStatus.OK)
    public List<TweetDTO> readValidatedByUser(@RequestParam String user, Pageable page) {
        return tweetService.readValidatedByUser(user, page);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TweetDTO readById(@PathVariable String id) {
        return tweetService.readById(id);
    }

    @PutMapping("/{id}/validate")
    @ResponseStatus(HttpStatus.OK)
    public TweetDTO validate(@PathVariable String id) {
        return tweetService.validate(id);
    }

}
