package com.kairosds.tuiter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HashtagDTO {

    private String id;
    private String value;
    private Long count;

}
