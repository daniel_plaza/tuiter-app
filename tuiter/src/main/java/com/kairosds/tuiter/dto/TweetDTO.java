package com.kairosds.tuiter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TweetDTO {

    private String id;
    private String user;
    private String text;
    private String localization;
    private boolean validated;

}
