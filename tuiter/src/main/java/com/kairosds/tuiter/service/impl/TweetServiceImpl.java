package com.kairosds.tuiter.service.impl;

import com.kairosds.tuiter.domain.Tweet;
import com.kairosds.tuiter.dto.TweetDTO;
import com.kairosds.tuiter.exception.TweetNotFoundException;
import com.kairosds.tuiter.repository.TweetRepository;
import com.kairosds.tuiter.service.TweetService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TweetServiceImpl implements TweetService {

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private MapperFacade mapper;

    public TweetDTO create(TweetDTO tweetDTO) {
        Tweet tweetToSave = mapper.map(tweetDTO, Tweet.class);

        Tweet tweetSaved = tweetRepository.save(tweetToSave);
        return mapper.map(tweetSaved, TweetDTO.class);
    }

    @Override
    public List<TweetDTO> readAll(Pageable page) {
        return tweetRepository.findAll(page).stream()
                .map(tweet -> mapper.map(tweet, TweetDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<TweetDTO> readValidatedByUser(String user, Pageable page) {
        return tweetRepository.findByUserAndValidated(user, true, page).stream()
                .map(tweet -> mapper.map(tweet, TweetDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public TweetDTO readById(String id) {
        Tweet tweet = getTweetById(id);
        return mapper.map(tweet, TweetDTO.class);
    }

    @Override
    public TweetDTO validate(String id) {
        Tweet tweet = getTweetById(id);
        tweet.setValidated(true);

        Tweet tweetUpdated = tweetRepository.save(tweet);
        return mapper.map(tweetUpdated, TweetDTO.class);
    }

    private Tweet getTweetById(String id) {
        return tweetRepository.findById(id)
                .orElseThrow(() -> new TweetNotFoundException(id));
    }

}
