package com.kairosds.tuiter.service;

import com.kairosds.tuiter.dto.TweetDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TweetService {

    TweetDTO create(TweetDTO tweetDTO);

    List<TweetDTO> readAll(Pageable page);

    List<TweetDTO> readValidatedByUser(String user, Pageable page);

    TweetDTO readById(String id);

    TweetDTO validate(String id);

}
