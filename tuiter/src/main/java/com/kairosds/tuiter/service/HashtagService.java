package com.kairosds.tuiter.service;

import com.kairosds.tuiter.dto.HashtagDTO;

import java.util.List;

public interface HashtagService {

    HashtagDTO add(String value);

    List<HashtagDTO> readFirst();

}
