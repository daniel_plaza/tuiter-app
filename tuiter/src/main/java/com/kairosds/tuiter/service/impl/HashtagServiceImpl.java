package com.kairosds.tuiter.service.impl;

import com.kairosds.tuiter.domain.Hashtag;
import com.kairosds.tuiter.dto.HashtagDTO;
import com.kairosds.tuiter.repository.HashtagRepository;
import com.kairosds.tuiter.service.HashtagService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HashtagServiceImpl implements HashtagService {

    @Autowired
    private HashtagRepository hashtagRepository;

    @Autowired
    private MapperFacade mapper;

    @Value("${twitter.hashtags}")
    private int hashtags;

    @Override
    public HashtagDTO add(String value) {
        Hashtag hashtag = hashtagRepository.findByValue(value)
                .orElseGet(() -> Hashtag.builder()
                        .value(value)
                        .build());
        hashtag.setCount(hashtag.getCount() + 1);

        Hashtag hashtagUpdated = hashtagRepository.save(hashtag);
        return mapper.map(hashtagUpdated, HashtagDTO.class);
    }

    @Override
    public List<HashtagDTO> readFirst() {
        return hashtagRepository.findByOrderByCountDesc(PageRequest.of(0, hashtags)).stream()
                .map(hashtag -> mapper.map(hashtag, HashtagDTO.class))
                .collect(Collectors.toList());
    }

}
