package com.kairosds.tuiter.configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    @Bean
    MapperFacade getMapperFacade() {
        return new DefaultMapperFactory.Builder().build().getMapperFacade();
    }

}
