package com.kairosds.tuiter.repository;

import com.kairosds.tuiter.domain.Hashtag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface HashtagRepository extends PagingAndSortingRepository<Hashtag, String> {

    Optional<Hashtag> findByValue(String value);

    List<Hashtag> findByOrderByCountDesc(Pageable page);

}
