package com.kairosds.tuiter.repository;

import com.kairosds.tuiter.domain.Tweet;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TweetRepository extends PagingAndSortingRepository<Tweet, String> {

    List<Tweet> findByUserAndValidated(String user, boolean validated, Pageable page);

}
