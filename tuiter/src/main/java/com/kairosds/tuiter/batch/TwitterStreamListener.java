package com.kairosds.tuiter.batch;

import com.kairosds.tuiter.dto.TweetDTO;
import com.kairosds.tuiter.service.HashtagService;
import com.kairosds.tuiter.service.TweetService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import twitter4j.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
@Profile("!test")
public class TwitterStreamListener implements ApplicationRunner {

    @Autowired
    private TweetService tweetService;

    @Autowired
    private HashtagService hashtagService;

    @Value("${twitter.languages}")
    private List<String> languages;

    @Value("${twitter.followers}")
    private int followers;

    @Override
    public void run(ApplicationArguments args) {
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(getStatusListener());
        twitterStream.filter(getTweetFilterQuery());
    }

    private FilterQuery getTweetFilterQuery() {
        return new FilterQuery()
                .locations(new double[][]{{-180, -90}, {180, 90}})
                .language(languages.toArray(new String[languages.size()]));
    }

    private StatusListener getStatusListener() {
        return new StatusListener() {

            @Override
            public void onException(Exception ex) {
            }

            @Override
            public void onStatus(Status status) {
                if (status.getUser().getFollowersCount() > followers) {
                    TweetDTO tweetDTO = TweetDTO.builder()
                            .user(status.getUser().getScreenName())
                            .localization(mapGeoLocation(status.getGeoLocation()))
                            .text(status.getText())
                            .build();

                    tweetService.create(tweetDTO);

                    Stream.of(status.getHashtagEntities())
                            .map(HashtagEntity::getText)
                            .forEach(hashtagService::add);
                }
            }

            private String mapGeoLocation(GeoLocation geoLocation) {
                return Optional.ofNullable(geoLocation)
                        .map(location -> location.getLatitude() + ", " + location.getLongitude())
                        .orElse(Strings.EMPTY);
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
            }

            @Override
            public void onStallWarning(StallWarning warning) {
            }

        };
    }

}
