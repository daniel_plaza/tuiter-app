const tokens = require('../resources/tokens');

module.exports = (req, res, next) => {
    if (req.headers.authorization && tokens.isAuthenticated(req.headers.authorization.replace('Bearer ', ''))) {
        next();
    } else {
        res.status(403).send('User not logged in. Forbidden access.');
    }
};