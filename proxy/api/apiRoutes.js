const express = require('express');
const router = express.Router();
const proxy = require('express-http-proxy');
const auth = require('./authentication');

const TUITER_API_URL = process.env.TUITER_API_URL || 'localhost:8080';

router.use(auth);

router.all('/*', async (req, res, next) => {
    console.log('Entrypoint:', req.url);
    next();
},
    proxy(TUITER_API_URL, {
        proxyReqPathResolver: async (req) => {
            var parts = req.url.split('?');
            var queryString = parts[1];
            var updatedPath = parts[0].replace(/\/api/, '');
            var redirectUrl = updatedPath + (queryString ? '?' + queryString : '');
            console.log('Redirecting to', TUITER_API_URL + redirectUrl);
            return redirectUrl;
        }
    })
);

module.exports = router;