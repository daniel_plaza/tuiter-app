const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json({ type: 'application/json' }));

app.use('/users', require('./user/userRoutes'));
app.use('/api', require('./api/apiRoutes'));

let port = 8090;

app.listen(port, () => {
    console.log('Server port ' + port);
});