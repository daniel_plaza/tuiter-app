const TokenList = module.exports = {
    tokens: [],
    add: function(token) {
        TokenList.tokens.push(token);
    },
    isAuthenticated:function(token){
    	return TokenList.tokens.includes(token);
    }
}