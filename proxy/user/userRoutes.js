const UserController = require('./userController');
const express = require('express');
const router = express.Router();

router.post('/login', async (req, res) => {
    if(req.body.username && req.body.password) {
        res.send(UserController.authenticate(req.body.username, req.body.password));
    } else {
        res.status(403).send('Invalid username or password');
    }
});

module.exports = router;
