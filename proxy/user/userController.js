const uuid = require('uuid/v1');
const tokens = require('../resources/tokens');

exports.authenticate = (user, password) => {
    var authentication = uuid();
    tokens.add(authentication);
    return authentication;
}
